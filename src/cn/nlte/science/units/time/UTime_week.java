package cn.nlte.science.units.time;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 时间单位：week，周
 *
 * @author yetao
 */
public class UTime_week extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 3600.0 * 24.0 * 7.0;
    private final static double additional = 0.0;
    private final static String description = "week";

    static {
        elementList.add(new UnitElement(BasicUnit.s, 1.0));
    }

    public UTime_week() {
        super(description, elementList, factor, additional);
    }

}
