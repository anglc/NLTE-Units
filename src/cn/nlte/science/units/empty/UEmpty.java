package cn.nlte.science.units.empty;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.length.ULength_in;
import java.util.ArrayList;
import java.util.List;

/**
 * �����λ��in2
 *
 * @author yetao
 */
public class UEmpty extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor=1.0;
    private final static double additional = 0.0;
    private final static String description = null;

    public UEmpty() {
        super(description, elementList, factor, additional);
    }

}
