package cn.nlte.science.units.energy;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.force.UForce_Kgf;
import java.util.ArrayList;
import java.util.List;

/**
 * 能量单位：Kgfm，千瓦时（度）
 *
 * @author yetao
 */
public class UEnergy_Kgfm extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "Kgfm";

    static {
        factor = new UForce_Kgf().getFactor();
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UEnergy_Kgfm() {
        super(description, elementList, factor, additional);
    }

}
