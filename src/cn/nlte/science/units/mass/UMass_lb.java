package cn.nlte.science.units.mass;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * ������λ��lb����
 *
 * @author yetao
 */
public class UMass_lb extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 0.45359237;
    private final static double additional = 0.0;
    private final static String description = "lb";

    static {
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
    }

    public UMass_lb() {
        super(description, elementList, factor, additional);
    }

}
