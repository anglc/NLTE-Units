package cn.nlte.science.units.mass;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 质量单位：ct，克拉
 *
 * @author yetao
 */
public class UMass_ct extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 0.0002;
    private final static double additional = 0.0;
    private final static String description = "ct";

    static {
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
    }

    public UMass_ct() {
        super(description, elementList, factor, additional);
    }

}
