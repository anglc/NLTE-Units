package cn.nlte.science.units.mass;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 质量单位：gr，格令
 *
 * @author yetao
 */
public class UMass_gr extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "gr";

    static {
        factor = new UMass_lb().getFactor() / 7000.0;
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
    }

    public UMass_gr() {
        super(description, elementList, factor, additional);
    }

}
