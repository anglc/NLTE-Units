package cn.nlte.science.units.mass;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * ������λ��qian��Ǯ
 *
 * @author yetao
 */
public class UMass_qian extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 0.005;
    private final static double additional = 0.0;
    private final static String description = "qian";

    static {
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
    }

    public UMass_qian() {
        super(description, elementList, factor, additional);
    }

}
