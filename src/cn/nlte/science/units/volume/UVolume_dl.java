package cn.nlte.science.units.volume;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 体积单位：dl，分升
 *
 * @author yetao
 */
public class UVolume_dl extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 0.0001;
    private final static double additional = 0.0;
    private final static String description = "dl";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 3.0));
    }

    public UVolume_dl() {
        super(description, elementList, factor, additional);
    }

}
