package cn.nlte.science.units.volume;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.length.ULength_mm;
import java.util.ArrayList;
import java.util.List;

/**
 * 体积单位：mm3，立方英尺
 *
 * @author yetao
 */
public class UVolume_mm3 extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "mm3";

    static {
        factor = Math.pow(new ULength_mm().getFactor(), 3.0);
        elementList.add(new UnitElement(BasicUnit.m, 3.0));
    }

    public UVolume_mm3() {
        super(description, elementList, factor, additional);
    }

}
