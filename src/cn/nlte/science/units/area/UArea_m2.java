package cn.nlte.science.units.area;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * �����λ��m2
 *
 * @author yetao
 */
public class UArea_m2 extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1.0;
    private final static double additional = 0.0;
    private final static String description = "m2";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
    }

    public UArea_m2() {
        super(description, elementList, factor, additional);
    }

}
