package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 长度单位：dm，分米
 *
 * @author yetao
 */
public class ULength_dm extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 0.1;
    private final static double additional = 0.0;
    private final static String description = "dm";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_dm() {
        super(description, elementList, factor, additional);
    }

}
