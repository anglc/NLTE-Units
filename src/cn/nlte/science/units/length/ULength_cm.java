package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * ���ȵ�λ��cm
 *
 * @author yetao
 */
public class ULength_cm extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 0.01;
    private final static double additional = 0.0;
    private final static String description = "cm";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_cm() {
        super(description, elementList, factor, additional);
    }

}
