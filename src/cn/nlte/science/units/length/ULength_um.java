package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 长度单位：um ，微米
 *
 * @author yetao
 */
public class ULength_um extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1.0E-6;
    private final static double additional = 0.0;
    private final static String description = "um";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_um() {
        super(description, elementList, factor, additional);
    }

}
