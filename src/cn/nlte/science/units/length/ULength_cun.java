package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 长度单位：cun，中国单位，1寸=0.1尺
 *
 * @author yetao
 */
public class ULength_cun extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "cun";

    static {
        factor = new ULength_chi().getFactor() * 0.1;
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_cun() {
        super(description, elementList, factor, additional);
    }

}
