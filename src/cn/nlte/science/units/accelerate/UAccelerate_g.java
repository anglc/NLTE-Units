package cn.nlte.science.units.accelerate;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 加速度单位：g，1重力加速度=9.80665 m/s^2
 *
 * @author yetao
 */
public class UAccelerate_g extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 9.80665;
    private final static double additional = 0.0;
    private final static String description = "g";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UAccelerate_g() {
        super(description, elementList, factor, additional);
    }

}
