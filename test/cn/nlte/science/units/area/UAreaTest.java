package cn.nlte.science.units.area;

import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.empty.UEmpty;
import cn.nlte.science.units.length.ULength_in;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author yetao
 */
public class UAreaTest {
    
    public UAreaTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UArea_m2());
        data = data.convertTo(new UArea_in2());
        assertEquals(Math.pow(1/25.4E-3,2), data.getValue(),1E-10);
    }

    @Test
    public void testAdd() {
        ValueWithUnit data1 = new ValueWithUnit(1, new UArea_m2());
        ValueWithUnit data2 = new ValueWithUnit(1, new UArea_in2());
        ValueWithUnit data = data1.add(data2).convertTo(new UArea_in2());
        assertEquals(Math.pow(1/25.4E-3,2)+1.0, data.getValue(),1E-10);
    }

    @Test
    public void testSub() {
        ValueWithUnit data1 = new ValueWithUnit(1, new UArea_m2());
        ValueWithUnit data2 = new ValueWithUnit(1, new UArea_in2());
        ValueWithUnit data = data1.sub(data2).convertTo(new UArea_in2());
        assertEquals(Math.pow(1/25.4E-3,2)-1.0, data.getValue(),1E-10);
    }

    @Test
    public void testMultiply() {
        ValueWithUnit data1 = new ValueWithUnit(1, new ULength_in());
        ValueWithUnit data2 = new ValueWithUnit(1, new ULength_in());
        ValueWithUnit data = data1.multiply(data2).convertTo(new UArea_in2());
        assertEquals(Math.pow(1.0,2), data.getValue(),1E-10);
    }

    @Test
    public void testDivide() {
        ValueWithUnit data1 = new ValueWithUnit(1, new UArea_m2());
        ValueWithUnit data2 = new ValueWithUnit(1, new UArea_in2());
        ValueWithUnit data = data1.divide(data2).convertTo(new UEmpty());
        assertEquals(Math.pow(1/25.4E-3,2), data.getValue(),1E-10);
    }
    
}
