package cn.nlte.science.units.force;

import cn.nlte.science.units.ValueWithUnit;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yetao
 */
public class UForceTest {

    public UForceTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UForce_KN());
        assertEquals(1, data.getValue(), 1E-10);
        data = data.convertTo(new UForce_N());
        assertEquals(1000, data.getValue(), 1E-10);
        data = data.convertTo(new UForce_dyn());
        assertEquals(1e+8, data.getValue(), 1E-6);
        data = data.convertTo(new UForce_tf());
        assertEquals(0.10197162, data.getValue(), 1E-6);
        data = data.convertTo(new UForce_lbf());
        assertEquals(224.808943, data.getValue(), 1E-5);
        data = data.convertTo(new UForce_UStf());
        assertEquals(0.11240447, data.getValue(), 1E-6);
        data = data.convertTo(new UForce_kip());
        assertEquals(0.22480894, data.getValue(), 1E-6);
        data = data.convertTo(new UForce_Kgf());
        assertEquals(101.971621, data.getValue(), 1E-5);
        data = data.convertTo(new UForce_gf());
        assertEquals(101971.621, data.getValue(), 1E-2);
    }

    @Test
    public void testAdd() {

    }

    @Test
    public void testSub() {

    }

    @Test
    public void testMultiply() {

    }

    @Test
    public void testDivide() {

    }

}
