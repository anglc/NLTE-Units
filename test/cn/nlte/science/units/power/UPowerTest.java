/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.nlte.science.units.power;

import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.energy.UEnergy_KWh;
import cn.nlte.science.units.time.UTime_h;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yetao
 */
public class UPowerTest {

    public UPowerTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UPower_KW());
        assertEquals(1, data.getValue(), 1E-10);
        data = data.convertTo(new UPower_W());
        assertEquals(1000, data.getValue(), 1E-10);
        data = data.convertTo(new UPower_Hp());
        assertEquals(1.34099997, data.getValue(), 1E-3);
        data = data.convertTo(new UPower_Ps());
        assertEquals(1.36000003, data.getValue(), 1E-3);
        data = data.convertTo(new UPower_KgfmPers());
        assertEquals(102.00000071, data.getValue(), 1E-1);
        data = data.convertTo(new UPower_KcalPers());
        assertEquals(0.2389, data.getValue(), 1E-4);
        data = data.convertTo(new UPower_calPers());
        assertEquals(238.9, data.getValue(), 1E-6);
        data = data.convertTo(new UPower_BtnPers());
        assertEquals(0.9478, data.getValue(), 1E-4);
        data = data.convertTo(new UPower_ftlbfPers());
        assertEquals(737.6, data.getValue(), 1E-1);
    }

    @Test
    public void testAdd() {

    }

    @Test
    public void testSub() {

    }

    @Test
    public void testMultiply() {

    }

    @Test
    public void testDivide() {
        ValueWithUnit energy = new ValueWithUnit(1, new UEnergy_KWh());
        ValueWithUnit time = new ValueWithUnit(1, new UTime_h());
        ValueWithUnit power = energy.divide(time);
        assertEquals(1000, power.convertTo(new UPower_W()).getValue(), 1E-8);

    }

}
